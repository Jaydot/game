#include "audio.h"
#include <SFML/Audio.hpp>
#include <string>

sf::Music currentSong;

Audio::Audio()
{

}

Audio::~Audio()
{
	currentSong.stop();
}

void Audio::init_music(std::string file_path)
{
	currentSong.openFromFile(file_path);
}

void Audio::play_song(bool loop)
{
	currentSong.play();
	currentSong.setLoop(loop);
}

void Audio::stop_song()
{
	currentSong.stop();
}
