#include "character.h" 

Character::Character(Ogre::String name, Ogre::SceneManager *sceneMgr, Ogre::Camera *cam)
{
	mName = name;
	mSceneMgr = sceneMgr;
	sinbadController = new SinbadCharacterController(cam, name);
}

Character::~Character()
{
	mSceneMgr->destroySceneNode(mName);
	if(sinbadController)
	{
		delete sinbadController;
		sinbadController = 0;
	}
}

void Character::setPosition(Ogre::Vector3 position)
{	
	sinbadController->setPosition(position);
}

void Character::injectKeyPressed(const OIS::KeyEvent& evt)
{
	sinbadController->injectKeyDown(evt);
}

void Character::injectKeyReleased(const OIS::KeyEvent& evt)
{
	sinbadController->injectKeyUp(evt);
}

void Character::injectMouseMoved(const OIS::MouseEvent& evt)
{
	sinbadController->injectMouseMove(evt);
}

void Character::injectMousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
	sinbadController->injectMouseDown(evt, id);
}

void Character::update(Ogre::Real elapsedTime, OIS::Keyboard *input)
{
	sinbadController->addTime(elapsedTime);	
}

void Character::setVisible(bool visible)
{
	sinbadController->setVisible(visible);
}
