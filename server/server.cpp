#include <iostream>
#include <SFML/Network.hpp>

const unsigned short PORT = 5000;
const std::string IPADDRESS("192.168.1.16");//change to suit your needs

sf::TcpListener listener;

sf::SocketSelector selector;

// this needs to be a list of sockets
sf::TcpSocket server_socket;
sf::TcpSocket player2_socket;

sf::Mutex globalMutex;

bool quit = false;

// this will need to change to be a list of all position data for each connected client
float x = 1683;
float y = 50;
float z = 2116;

float x2 = 1683;
float y2 = 50;
float z2 = 2166;

void DoStuff(void)
{
    while(!quit)
    {
	if(selector.wait())
        {
           if(selector.isReady(server_socket))
           {
		sf::Packet packet_recieve;
		server_socket.receive(packet_recieve);

		std::string command;
		float posx, posy, posz;
		globalMutex.lock();
			packet_recieve >> command >> posx >> posy >> posz;
		globalMutex.unlock();

		// later we'll check to see what command was passed to us
		// for now we know they want the position because that's all we have
		if(command == "position")
		{
        		sf::Packet packetSend;
			// make sure nobody is touching the data while we're
			// trying to send it
       			globalMutex.lock();
				// populate the packet
       				packetSend << x << y << z << x2 << y2 << z2;
       			globalMutex.unlock();

			// send the packet
       			server_socket.send(packetSend);
		}
		else if(command == "move")
		{
			x = posx;
			y = posy;
			z = posz;
		}
            }
	   else if(selector.isReady(player2_socket))
           {
		sf::Packet packet_recieve;
		player2_socket.receive(packet_recieve);

		std::string command;
		float posx, posy, posz;
		globalMutex.lock();
			packet_recieve >> command >> posx >> posy >> posz;
		globalMutex.unlock();

		// later we'll check to see what command was passed to us
		// for now we know they want the position because that's all we have
		if(command == "position")
		{
        		sf::Packet packetSend;

			// make sure nobody is touching the data while we're
			// trying to send it
        		globalMutex.lock();
				// populate the packet
        			packetSend << x2 << y2 << z2 << x << y << z;
        		globalMutex.unlock();

			// send the packet
        		player2_socket.send(packetSend);
		}
		else if(command == "move")
		{
			x2 = posx;
			y2 = posy;
			z2 = posz;
		}
            }
        }
    }
}
void Server(void)
{
    // wait until someone connects
    // 'server_socket' is the socket connecting the server to the client
    // the name should probably be changed
    //listener.accept(server_socket);

    listener.accept(player2_socket);
    std::cout << "New client connected: " << player2_socket.getRemoteAddress() << std::endl;

    selector.add(player2_socket);
}

void GetInput(void)
{
    std::string s;
    std::cout << "\nEnter \"exit\" to quit or message to send: \n";
    std::cin >> s;
 
    if(s == "exit")
        quit = true;
    if(s == "status")
    {
	std::cout << "\nPlayer 1 info: x:" << x << ", y:" << y << ", z:" << z;
    	std::cout << "\nPlayer 2 info: x:" << x2 << ", y:" << y2 << ", z:" << z2;
	}
}

int main(int argc, char* argv[])
{
    sf::Thread* thread = 0;
    sf::Thread* thread2 = 0;

    // listen on the specified port
    listener.listen(PORT);
    listener.accept(server_socket);

    std::cout << "New client connected: " << server_socket.getRemoteAddress() << std::endl;

    selector.add(server_socket);

    // needed threads
    thread2 = new sf::Thread(&Server);
    thread2->launch();

    // waiting for connecting clients
    // processing client interaction
    thread = new sf::Thread(&DoStuff);
    thread->launch();

    while(!quit)
    {
        GetInput();
    }
    if(thread)
    {
        thread->wait();
        delete thread;
    }
    if(thread2)
    {
       thread2->wait();
       delete thread2;
    }
    return 0;
}
