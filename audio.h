#ifndef _AUDIO_H
#define _ADUIO_H

#include <string>

class Audio
{
	public:
		Audio();
		~Audio();
		void init_music(std::string file_path);
		void play_song(bool loop);
		void stop_song();
};

#endif
