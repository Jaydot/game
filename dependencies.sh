# comment out whatever you don't want
# min requirements are opengl, glm, glew, and sfml
yum -y update
yum groupinstall "Development Tools"
yum install gcc-c++
yum install glm-devel
yum install glew-devel
yum install vim
yum install cmake
# sfml dependencies
# might be more than needed, evaluate later
yum install libXmu-devel
yum install libXi-devel
yum install libjpeg-devel
yum install libsndfile-devel
yum install libXrandr-devel
yum install openal-devel
yum install freetype-devel
yum install doxygen

git clone https://github.com/LaurentGomila/SFML.git
cd SFML
mkdir SFML-build
cd SFML-build
cmake -DCMAKE_INSTALL_PREFIX=/usr ".." -DSFML_BUILD_DOC=true -DSFML_BUILD_EXAMPLES=true
make
make doc
sudo make DESTDIR="/" install
