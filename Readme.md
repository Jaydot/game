The goal of this project is to create a basic game engine in c++ that utilizes lua for scripting.

Content will be done in lua to allow for content changes without a rebuild.
UI layout will also be done via lua.

This engine will be focused around rpgs and may be expanded later.

Will now be using Ogre3d for the graphics portion.

Dependencies:
	Ogre 1.9
	SFML 2.0
	
