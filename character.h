#ifndef _CHARACTER_H
#define _CHARACTER_H

// include ogre references 
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>


#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include "SinbadCharacterController.h"

class Character
{
	protected:
		Ogre::SceneManager *mSceneMgr;
		Ogre::String mName;
		SinbadCharacterController* sinbadController;
		
	public:
		void update (Ogre::Real elapsedTime, OIS::Keyboard *input);

		// The three methods below returns the two camera-related nodes,
		// and the current position of the character (for the 1st person camera
		Ogre::SceneNode *getSightNode()
		{
			return sinbadController->getCameraGoal();
		}

		Ogre::SceneNode *getCameraNode()
		{
			return sinbadController->getCameraNode();
		}

		Ogre::Vector3 getWorldPosition()
		{
			return sinbadController->getBodyNode()->_getDerivedPosition();
		}
		
		Character(Ogre::String name, Ogre::SceneManager *sceneMgr, Ogre::Camera *cam);
		~Character();
		void setVisible(bool visible);
		void setPosition(Ogre::Vector3 Position);
		void injectKeyPressed(const OIS::KeyEvent& evt);
		void injectKeyReleased(const OIS::KeyEvent& evt);
		void injectMouseMoved(const OIS::MouseEvent& evt);
		void injectMousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
};

#endif
